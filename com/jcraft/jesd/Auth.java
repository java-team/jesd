/* JEsd
 * Copyright (C) 1999 JCraft Inc.
 *  
 * Written by: 1999 ymnk
 *   
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
   
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.jcraft.jesd;

class Auth{
  static final int ESD_KEY_LEN=16;
  static byte[] esd_key = null;
  static void init(String key){
    if(key!=null){
//    if(key.length()!=ESD_KEY_LEN){
//    }
      esd_key = key.getBytes();
    }
  }
  synchronized static boolean auth(byte[] key){
    if(esd_key==null){
      esd_key = new byte[ESD_KEY_LEN];
      for(int i=0;i<ESD_KEY_LEN;i++){
	esd_key[i]=key[i];
      }
      return true;
    }
    else{
      for(int i=0;i<ESD_KEY_LEN;i++){
	if(esd_key[i]!=key[i])return false;
      }
      return true;
    }
  }
  static boolean auth(IO io){
    boolean result=true;
    try{
      byte[] buff = new byte[ESD_KEY_LEN];
      io.readByte(buff);  // auth
      io.readPad(4);      // endian
      io.writeInt(1);     // ok
      result=auth(buff);
    }
    catch(Exception e){
      result=false;
    }
    return result;
  }
}
