/* JEsd
 * Copyright (C) 1999 JCraft Inc.
 *  
 * Written by: 1999 ymnk
 *   
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
   
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.jcraft.jesd;

import java.util.Vector;
abstract class Player{
  int id=0;
  static private Vector pool=new Vector();
  static protected synchronized void add(Player p){ pool.addElement(p); }
  static protected synchronized void del(Player p){ pool.removeElement(p); }
  static private int _id=1;
  static synchronized int getId(){ return _id++; }
  static synchronized void resetId(){ _id=1; }
  static synchronized Player get(int id){
    int size=pool.size();
    Player p;
    for(int i=0; i<size; i++){
      p=(Player)(pool.elementAt(i));
      if(p.id==id) return p;
    }
    return null;
  }

  static synchronized void dumpInfo(IO io) throws java.io.IOException{
    int size=pool.size();
    Player p;
    StreamPlayer sp;
    for(int i=0; i<size; i++){
      p=(Player)(pool.elementAt(i));
      if(p instanceof StreamPlayer){
        sp=(StreamPlayer)p;
        io.writeInt(sp.id);
        io.writeByte(sp.name.getBytes());
        io.writeInt(sp.rate);
        io.writeInt(sp.left_vol_scale);
        io.writeInt(sp.right_vol_scale);
        io.writeInt(sp.format);
      }
    }
  }

  Player(){this.id=getId();}
}
